import ForceGraph from "force-graph";
import axios from "axios";
import store from "../store";

const backendEndpoint =
  "https://compreqs-server.brazilsouth.cloudapp.azure.com/neo4j/requisitos";

export async function getGraphs(params) {
  const gData = await fetchGraphs(params);
  const transformedGData = transformGraph(gData);

  return buildGraph(transformedGData);
}

async function fetchGraphs(params) {
  const response = await axios.get(backendEndpoint, { params });

  return response.data.results;
}

function buildGraph(gData) {
  const forceGraph = ForceGraph()(document.getElementById("graph"))
    .nodeCanvasObject(({ x, y, name, code }, ctx) => {
      const label = limitNameSize(name);
      ctx.fillStyle =
        gData.centers.filter(c => c.code === code).length > 0
          ? "#F6AE2D"
          : "#40a8c4";

      ctx.beginPath();
      ctx.arc(x, y, 3, 0, 2 * Math.PI, false);
      ctx.fill();

      ctx.fillStyle = "black";
      ctx.font = `3px 'Montserrat'`;
      ctx.textAlign = "center";
      ctx.textBaseline = "middle";
      ctx.fillText(label, x, y + 6);
    })
    .graphData(gData)
    .linkDirectionalArrowLength(4)
    .nodeId("code")
    .nodeLabel(({ code, name }) => `${code} - ${name}`)
    .onNodeClick(({ code, name }) => {
      store.dispatch("showSubjectCard");
      store.commit("changeSubjectCardData", { code, name });
    })
    .centerAt(0, 0)
    .zoom(4);

  return forceGraph;
}

function limitNameSize(name) {
  return name.length >= 30 ? name.slice(0, 30) + "..." : name;
}

function transformGraph(graph) {
  const transformedGraph = {
    centers: graph.centers,
    nodes: graph.nodes,
    links: transformLinks(graph.links)
  };

  return transformedGraph;
}

function transformLinks(links) {
  const transformedLinks = [];

  links.forEach(link => {
    const transformedLink = {
      properties: link.properties,
      source: link.target,
      target: link.source
    };

    transformedLinks.push(transformedLink);
  });

  return transformedLinks;
}

import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    graph: null,
    depth: "",
    input: "",
    searchSubmitted: false,
    isSearching: false,
    showSubjectCard: false,
    subjectCardData: {
      code: undefined,
      name: undefined
    }
  },
  mutations: {
    changeGraph(state, newGraph) {
      state.graph = newGraph;
    },
    changeDepth(state, newDepth) {
      state.depth = newDepth;
    },
    changeInput(state, newInput) {
      state.input = newInput;
    },
    changeSearchSubmitted(state, newSearchSubmittedVal) {
      state.searchSubmitted = newSearchSubmittedVal;
    },
    changeIsSearching(state, newIsSearchingVal) {
      state.isSearching = newIsSearchingVal;
    },
    changeShowSubjectCard(state, newShowSubjectCard) {
      state.showSubjectCard = newShowSubjectCard;
    },
    changeSubjectCardData(state, newSubjectCardData) {
      state.subjectCardData = newSubjectCardData;
    }
  },
  actions: {
    submitSearch({ commit }) {
      commit("changeSearchSubmitted", true);
      commit("changeIsSearching", true);
    },
    searchDone({ commit }) {
      commit("changeSearchSubmitted", false);
      commit("changeIsSearching", false);
    },
    showSubjectCard({ commit }) {
      commit("changeShowSubjectCard", true);
    },
    hideSubjectCard({ commit }) {
      commit("changeShowSubjectCard", false);
    },
    resetSubjectCardData({ commit }) {
      commit("changeSubjectCardData", {
        code: undefined,
        name: undefined
      });
    }
  }
});

export default store;

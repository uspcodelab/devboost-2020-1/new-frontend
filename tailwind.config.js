module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true
  },
  purge: [],
  theme: {
    extend: {},
    fill: theme => ({
      "gray-100": theme("colors.gray.100"),
      "gray-900": theme("colors.gray.900")
    })
  },
  variants: {},
  plugins: []
};
